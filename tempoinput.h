/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_TEMPOINPUT_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_TEMPOINPUT_HFILE

/***
HEADER FILE:
Tempo Input
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

class TempoInput
{
    private:
        int iTx100, iTx10, iTx1;

    public:
        TempoInput();
        ~TempoInput();

        void init();

        void setTempox100(int);
        void setTempox10(int);
        void setTempox1(int);

        int getTempox100();
        int getTempox10();
        int getTempox1();

        int getValue();
};

#endif
