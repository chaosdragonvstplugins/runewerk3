/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "runewerk_synth.h"

/***
C++ FILE:
Rectangle Wave Synthesizer
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

RuneWerkRechteckSynth::RuneWerkRechteckSynth()
{
}

RuneWerkRechteckSynth::~RuneWerkRechteckSynth()
{
}

void RuneWerkRechteckSynth::Init(float fs)
{
    clRuneWerkRechteckOsc.Init(fs);
}

void RuneWerkRechteckSynth::setSampleRate(float fs)
{
    clRuneWerkRechteckOsc.setSampleRate(fs);
}

void RuneWerkRechteckSynth::setMode(int m)
{
    clRuneWerkRechteckOsc.setMode(m);
}

void RuneWerkRechteckSynth::setPulseWidth(float v)
{
    clRuneWerkRechteckOsc.setPulseWidth(v);
}

void RuneWerkRechteckSynth::setFreq(float v)
{
    clRuneWerkRechteckOsc.setFreq(v);
}

void RuneWerkRechteckSynth::setFineTune(float v)
{
    clRuneWerkRechteckOsc.setFineTune(v);
}

void RuneWerkRechteckSynth::setMasterFineTune(float v)
{
    clRuneWerkRechteckOsc.setSecondFineTune(v);
}

void RuneWerkRechteckSynth::setGain(float v)
{
    fGain = v;
}

void RuneWerkRechteckSynth::setVel(int v){fVel = ((float)v) * INV_127;}

float RuneWerkRechteckSynth::getSampleRate(){return clRuneWerkRechteckOsc.getSampleRate();}

int RuneWerkRechteckSynth::getMode(){return clRuneWerkRechteckOsc.getMode();}
float RuneWerkRechteckSynth::getPulseWidth(){return clRuneWerkRechteckOsc.getPulseWidth();}
float RuneWerkRechteckSynth::getFreq(){return clRuneWerkRechteckOsc.getFreq();}
float RuneWerkRechteckSynth::getFineTune(){return clRuneWerkRechteckOsc.getFineTune();}
float RuneWerkRechteckSynth::getMasterFineTune(){return clRuneWerkRechteckOsc.getSecondFineTune();}
float RuneWerkRechteckSynth::getGain(){return fGain;}

void RuneWerkRechteckSynth::getOutputSample(float *outL, float *outR)
{
        (*outL) =  clRuneWerkRechteckOsc.getOutputSample() * fGain * fVel;
        (*outR) = (*outL);
}
