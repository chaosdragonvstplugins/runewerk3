/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef VERSION_H
#define VERSION_H

namespace AutoVersion{

	//Date Version Types
	static const char DATE[] = "31";
	static const char MONTH[] = "10";
	static const char YEAR[] = "2019";
	static const char UBUNTU_VERSION_STYLE[] =  "19.10";

	//Software Status
	static const char STATUS[] =  "Thurisaz";
	static const char STATUS_SHORT[] =  "th";

	//Standard Version Type
	static const long MAJOR  = 3;
	static const long MINOR  = 1;
	static const long BUILD  = 0;
	static const long REVISION  = 0;

	//Miscellaneous Version Types
	static const long BUILDS_COUNT  = 25;
	#define RC_FILEVERSION 3,1,0,0
	#define RC_FILEVERSION_STRING "3, 1, 0, 0\0"
	static const char FULLVERSION_STRING [] = "3.1.0.0";

	static const long BUILD_HISTORY  = 0;
}
#endif
