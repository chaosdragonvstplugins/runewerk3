/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "lfo.h"

/***
C++ FILE:
Low Frequency Oscillator
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

LowFreqOsc::LowFreqOsc()
{
}

LowFreqOsc::~LowFreqOsc()
{
}

void LowFreqOsc::init(float v)
{
    setEnabled(false);
    setSampleRate(v);
    setBPM(120);
    setSyncRateE(LFOR_1OVER1);
    setMinAmount(0.0f);
    setMaxAmount(1.0f);

    fPhase = 0.0f;
}

void LowFreqOsc::resetPhase()
{
    fPhase = 0.0f;
}

void LowFreqOsc::setEnabled(bool e){isOn = e;}
void LowFreqOsc::setSampleRate(float v){fSampleRate = v; fTwoPiOverSampleRate = TWO_PI / fSampleRate;}
void LowFreqOsc::setBPM(int v){clSync.setBPM(v); fRate = clSync.getRealRate();}
void LowFreqOsc::setRateNumerator(int v){clSync.setBeatSyncNumerator(v); fRate = clSync.getRealRate();}
void LowFreqOsc::setRateDenominator(int v){clSync.setBeatSyncDenominator(v); fRate = clSync.getRealRate();}

void LowFreqOsc::setSyncRateE(int v)
{
    eLFORate = v >= LFOR_NUMRATES ? LFOR_1OVER1 : v;

    switch(eLFORate)
    {
        case LFOR_32OVER1:
            setRateNumerator(32);
            setRateDenominator(1);
            break;

        case LFOR_16OVER1:
            setRateNumerator(16);
            setRateDenominator(1);
            break;

        case LFOR_8OVER1:
            setRateNumerator(8);
            setRateDenominator(1);
            break;

        case LFOR_4OVER1:
            setRateNumerator(4);
            setRateDenominator(1);
            break;

        case LFOR_2OVER1:
            setRateNumerator(2);
            setRateDenominator(1);
            break;

        case LFOR_1OVER1:
            setRateNumerator(1);
            setRateDenominator(1);
            break;

        case LFOR_1OVER2:
            setRateNumerator(1);
            setRateDenominator(2);
            break;

        case LFOR_3OVER4:
            setRateNumerator(3);
            setRateDenominator(4);
            break;

        case LFOR_1OVER4:
            setRateNumerator(1);
            setRateDenominator(4);
            break;

        case LFOR_5OVER8:
            setRateNumerator(5);
            setRateDenominator(8);
            break;

        case LFOR_3OVER8:
            setRateNumerator(3);
            setRateDenominator(8);
            break;

        case LFOR_1OVER8:
            setRateNumerator(1);
            setRateDenominator(8);
            break;

        case LFOR_7OVER16:
            setRateNumerator(7);
            setRateDenominator(16);
            break;

        case LFOR_5OVER16:
            setRateNumerator(5);
            setRateDenominator(16);
            break;

        case LFOR_3OVER16:
            setRateNumerator(3);
            setRateDenominator(16);
            break;

        case LFOR_1OVER16:
            setRateNumerator(1);
            setRateDenominator(16);
            break;

        case LFOR_3OVER32:
            setRateNumerator(3);
            setRateDenominator(32);
            break;

        case LFOR_1OVER32:
            setRateNumerator(1);
            setRateDenominator(32);
            break;

        case LFOR_1OVER4T:
            setRateNumerator(1);
            setRateDenominator(3);
            break;

        case LFOR_1OVER8T:
            setRateNumerator(1);
            setRateDenominator(6);
            break;
    }
}

void LowFreqOsc::setMinAmount(float v){fMinAmt = v;}
void LowFreqOsc::setMaxAmount(float v){fMaxAmt = v;}

bool LowFreqOsc::getEnabled(){return isOn;}
float LowFreqOsc::getSampleRate(){return fSampleRate;}
int LowFreqOsc::getBPM(){return clSync.getBPM();}
int LowFreqOsc::getRateNumerator(){return clSync.getBeatSyncNumerator();}
int LowFreqOsc::getRateDenominator(){return clSync.getBeatSyncDenominator();}
int LowFreqOsc::getSyncRateE(){return eLFORate;}
float LowFreqOsc::getMinAmount(){return fMinAmt;}
float LowFreqOsc::getMaxAmount(){return fMaxAmt;}

float LowFreqOsc::getValue()
{
    float y = fPhase * TWO_OVER_PI;

    if(y > 0.0f)
        y *= +0.25f;
    else
        y *= -0.25f;

    fPhase += (fTwoPiOverSampleRate * fRate);

    if(fPhase >= PI)
        fPhase -= TWO_PI;

    return (y * (fMaxAmt - fMinAmt) + fMinAmt * 0.5f) * 2;
}
