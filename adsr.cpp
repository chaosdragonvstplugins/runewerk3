/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "adsr.h"

/***
C++ FILE:
Signal Envelope
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

ADSREnvelope::ADSREnvelope()
{
    eStage = RWCENV_STAGE_FINISHED;
    fTime = 0.0f;
    fSampleRate = 44100.0f;
    fInvSampleRate = 1.0f / fSampleRate;
    fSampleZ = 0.0f;
    isSustainLoopOn = false;
    fAttack = 0.1f; fInvAttack = 1.0f / fAttack;
    fDecay = 0.5f; fInvDecay = 1.0f / fDecay;
    fSustain = 0.0f;
    fRelease = 1.0f; fInvRelease = 1.0f / fRelease;
}

void ADSREnvelope::Reset()
{
    eStage = RWCENV_STAGE_ATTACK;
    fTime = 0.0f;
    isSustainLoopOn = false;
}

void ADSREnvelope::setSampleRate(float fs){fSampleRate = fs; fInvSampleRate = 1.0f / fSampleRate;}
void ADSREnvelope::setAttack(float a){fAttack = a; fInvAttack = 1.0f / fAttack;}
void ADSREnvelope::setDecay(float d){fDecay = d; fInvDecay = 1.0f / fDecay;}
void ADSREnvelope::setSustain(float s){fSustain = s;}
void ADSREnvelope::setRelease(float r){fRelease = r; fInvRelease = 1.0f / fRelease;}
void ADSREnvelope::setStage(int e){fTime = 0.0f; eStage = e;}
void ADSREnvelope::setSustainLoopEnabled(bool x){isSustainLoopOn = x;}
void ADSREnvelope::setFinished(){setStage(RWCENV_STAGE_FINISHED);}
void ADSREnvelope::setReleaseStage(){setStage(RWCENV_STAGE_RELEASE);}

float ADSREnvelope::getAttack(){return fAttack;}
float ADSREnvelope::getDecay(){return fDecay;}
float ADSREnvelope::getSustain(){return fSustain;}
float ADSREnvelope::getRelease(){return fRelease;}
int ADSREnvelope::getStage(){return eStage;}
bool ADSREnvelope::getFinished(){return (getStage() == RWCENV_STAGE_FINISHED);}

float ADSREnvelope::getSample()
{
    switch(eStage)
    {
        case RWCENV_STAGE_ATTACK:
            if(fTime >= fAttack || fSample >= 1.0f)
            {
                fSample = 1.0f;
                eStage = RWCENV_STAGE_DECAY;
                fTime = 0.0f;
            }
            else
            {
                fSample = fInvAttack * fTime;
                fTime += fInvSampleRate;
            }

            fSampleZ = fSample;
            break;

        case RWCENV_STAGE_DECAY:
            if(fTime >= fAttack + fDecay || fSample <= fSustain)
            {
                fSample = fSustain;
                eStage = RWCENV_STAGE_SUSTAIN;
                fTime = 0.0f;
            }
            else
            {
                fSample = (fSustain - 1.0f) * fInvDecay * fTime + 1.0f;
                fTime += fInvSampleRate;
            }

            fSampleZ = fSample;
            break;

        case RWCENV_STAGE_SUSTAIN:
            if(isSustainLoopOn)
            {
                fSample = fSustain;
                fTime = 0.0f;
            }
            else
            {
                eStage = RWCENV_STAGE_RELEASE;
                fTime = 0.0f;
            }

            fSampleZ = fSample;
            break;

        case RWCENV_STAGE_RELEASE:
            if(fSample <= 0.0f)
            {
                fSampleZ = 0.0f;
                fSample = 0.0f;
                eStage = RWCENV_STAGE_FINISHED;
                fTime = 0.0f;
            }
            else
            {
                fSample = -fSampleZ * fInvRelease * fTime + fSampleZ;
                fTime += fInvSampleRate;
            }
            break;

        case RWCENV_STAGE_FINISHED:
            fSample = 0.0f;
            fSampleZ = 0.0f;
            fTime = 0.0f;
            break;
    }

    return fSample;
}
