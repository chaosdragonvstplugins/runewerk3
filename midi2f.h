/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_MIDITOFREQ_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_MIDITOFREQ_HFILE

/***
HEADER FILE:
MIDI to Frequency conversion
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

#include <cmath>

float midi2freq(int);
float midi2hextone(int);

#endif
