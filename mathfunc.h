/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_MATHFUNC_CLASS_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_MATHFUNC_CLASS_HFILE

/***
HEADER FILE:
Math Functions
for RuneWerk synthesizer plugins
(c) Luigi Elettrico
***/

#include <cmath>

#define PI          3.14159265f
#define PI_OVER_2   1.57079632f
#define TWO_PI      6.28318531f
#define TWO_OVER_PI 0.63661977f

#define HTONE       1.05946309f
#define OVERHT      0.94387431f

#define INV_3       0.33333333f
#define INV_9       0.11111111f
#define INV_60      0.01666666f
#define INV_127     0.00787402f

class MathFunc
{
    public:
        MathFunc();
        ~MathFunc();

        bool FloatToBool(float);
        float BoolToFloat(bool);

        float Abs(float);

        float Min(float, float);
        float Max(float, float);

        float Pow(float, float);
        float Ln(float);
        float Log(float);

        float V2dB(float);
        float dB2V(float);

        float Exp10Scale(float, float, float);
        float Log10Scale(float, float, float);
};

#endif
