/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_RECHTECK_OSC_CLASS_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_RECHTECK_OSC_CLASS_HFILE

/***
HEADER FILE:
Rectangle Wave Oscillator
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

#include "mathfunc.h"

#define RWRO_MODE_PULSE     0
#define RWRO_MODE_NOISE     1

class RuneWerkRechteckOsc
{
    private:
        float p_fSampleFreq, p_fOneOverFs, p_fTwoPiOvrFs;
        float p_fFreq, p_fVel, p_fFineTune, p_fSecondFineTune, p_fRealDetune, p_fSecondRealDetune;
        float p_fPulseWidth;
        float p_fPhase;

        int p_iMode;

        unsigned int R;
        unsigned int P, N, V, t;

        unsigned int getLFSR32();

    public:
        RuneWerkRechteckOsc();
        ~RuneWerkRechteckOsc();

        void Init(float fs);

        void setMode(int v);
        void setSampleRate(float fs);
        void setFineTune(float v);
        void setSecondFineTune(float v);
        void setPulseWidth(float v);
        void setFreq(float v);

        int getMode();
        float getSampleRate();
        float getFineTune();
        float getSecondFineTune();
        float getPulseWidth();
        float getFreq();

        float getOutputSample();
};

#endif
