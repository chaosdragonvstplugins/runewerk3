/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "runewerk_osc.h"

/***
C++ FILE:
Rectangle Wave Oscillator
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

RuneWerkRechteckOsc::RuneWerkRechteckOsc()
{
}

RuneWerkRechteckOsc::~RuneWerkRechteckOsc()
{
}

unsigned int RuneWerkRechteckOsc::getLFSR32()
{
    R = (R >> 1) ^ (unsigned int)((0 - (R & 1u)) & 0xd0000001u);

    return R;
}

void RuneWerkRechteckOsc::Init(float fs)
{
    setSampleRate(fs);
    setFreq(0.0f);
    setFineTune(0.0f);
    setSecondFineTune(0.0f);

    p_fPhase = 0.0f;

    R = 1;
    P = 0;
    N = 0;
    V = 0;
    t = 0;
}

void RuneWerkRechteckOsc::setMode(int v)
{
    p_iMode = v & 1;
}

void RuneWerkRechteckOsc::setSampleRate(float fs)
{
    p_fSampleFreq = fs;
    p_fOneOverFs = 1.0f / p_fSampleFreq;
    p_fTwoPiOvrFs = TWO_PI / p_fSampleFreq;
}

void RuneWerkRechteckOsc::setFineTune(float v)
{
    p_fFineTune = v;
    p_fRealDetune = (0.5f * (p_fFineTune + 1.0f)) * (HTONE - OVERHT) + OVERHT;
}

void RuneWerkRechteckOsc::setSecondFineTune(float v)
{
    p_fSecondFineTune = v;
    p_fSecondRealDetune = (0.5f * (p_fSecondFineTune + 1.0f)) * (HTONE - OVERHT) + OVERHT;
}

void RuneWerkRechteckOsc::setPulseWidth(float v)
{
    p_fPulseWidth = v;
}

void RuneWerkRechteckOsc::setFreq(float v)
{
    p_fFreq = v;
}

int RuneWerkRechteckOsc::getMode(){return p_iMode;}

float RuneWerkRechteckOsc::getSampleRate(){return p_fSampleFreq;}

float RuneWerkRechteckOsc::getFineTune(){return p_fFineTune;}
float RuneWerkRechteckOsc::getSecondFineTune(){return p_fFineTune;}
float RuneWerkRechteckOsc::getPulseWidth(){return p_fPulseWidth;}
float RuneWerkRechteckOsc::getFreq(){return p_fFreq;}

float RuneWerkRechteckOsc::getOutputSample()
{
    float y = 0.0f;

    switch(p_iMode)
    {
        case RWRO_MODE_PULSE:
            p_fPhase += (p_fTwoPiOvrFs * p_fFreq * p_fRealDetune * p_fSecondRealDetune);

            if(p_fPhase >= TWO_PI)
                p_fPhase -= TWO_PI;

            if(p_fPhase >= TWO_PI * p_fPulseWidth)
                y = 0.0f;
            else
                y = 1.0f;
            break;

        case RWRO_MODE_NOISE:
            if(P == 1)
			{
				N = getLFSR32() & 1;
			}

			if(((int)(8.0f * (float)(++t) * p_fFreq * p_fRealDetune * p_fOneOverFs) & 1) == 1)
			{
				V = N;
				P = 0;
			}
			else
			{
				P = 1;
			}

			y = (float)V;
            break;
    }

    return y;
}
