# RuneWerk 3 VST

A simple oldschool-sounding pulse, pwm & noise wave synthesizer with over 20 parameters, written in C++.
Demo song of the plugin available here:
https://youtu.be/L64igUqxzdw
