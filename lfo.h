/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_LFO_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_LFO_HFILE

/***
HEADER FILE:
Low Frequency Oscillator, triangle wave
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

#include "bpmsync.h"

enum eLFOSyncRate {
    LFOR_32OVER1,
    LFOR_16OVER1,
    LFOR_8OVER1,
    LFOR_4OVER1,
    LFOR_2OVER1,
    LFOR_1OVER1,
    LFOR_3OVER4,
    LFOR_5OVER8,
    LFOR_1OVER2,
    LFOR_7OVER16,
    LFOR_3OVER8,
    LFOR_5OVER16,
    LFOR_1OVER4T,
    LFOR_1OVER4,
    LFOR_3OVER16,
    LFOR_1OVER8T,
    LFOR_1OVER8,
    LFOR_3OVER32,
    LFOR_1OVER16,
    LFOR_1OVER32,

    LFOR_NUMRATES
};

class LowFreqOsc
{
    private:
        BPMSync clSync;

        int eLFORate;

        bool isOn;
        float fSampleRate, fTwoPiOverSampleRate;
        float fRate;
        float fMinAmt, fMaxAmt;

        float fPhase;

    public:
        LowFreqOsc();
        ~LowFreqOsc();

        void init(float);
        void resetPhase();

        void setEnabled(bool);
        void setSampleRate(float);
        void setBPM(int);
        void setRateNumerator(int);
        void setRateDenominator(int);
        void setSyncRateE(int);
        void setMinAmount(float);
        void setMaxAmount(float);

        bool getEnabled();
        float getSampleRate();
        int getBPM();
        int getRateNumerator();
        int getRateDenominator();
        int getSyncRateE();
        float getMinAmount();
        float getMaxAmount();

        float getValue();
};

#endif
