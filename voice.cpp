/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "voice.h"

/***
C++ FILE:
Rectangle Synthesizer Voice
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

void RuneWerkRechteckVoice::init(float fs)
{
    fSmpFreq = fs;

    clRuneWerkRechteck.Init(fSmpFreq);
    clEnv.setSampleRate(fSmpFreq);
    clEnvFilter.init(fSmpFreq);
    clPWMLFO.init(fSmpFreq);
    clTremoloLFO.init(fSmpFreq);
    clVibratoLFO.init(fSmpFreq);

    isNoteOn = false;

    clEnv.setAttack(0.05f);
    clEnv.setDecay(0.5f);
    clEnv.setSustain(0.8f);
    clEnv.setRelease(0.6f);
    clEnv.Reset();

    clEnvFilter.setAttack(0.005f);
    clEnvFilter.setDecay(0.2f);
    clEnvFilter.setSustain(0.6f);
    clEnvFilter.setRelease(0.5f);
    clEnvFilter.Reset();

    clRuneWerkRechteck.setMode(RWRO_MODE_PULSE);
    clRuneWerkRechteck.setPulseWidth(0.5f);
    clRuneWerkRechteck.setGain(1.0f);

    clEnvFilter.setCutoff(2000.0f);
    clEnvFilter.setReso(1.0f);

    clPWMLFO.setMinAmount(0.0f);
    clPWMLFO.setMaxAmount(1.0f);
    clPWMLFO.setEnabled(false);

    clTremoloLFO.setMinAmount(0.0f);
    clTremoloLFO.setMaxAmount(1.0f);
    clTremoloLFO.setEnabled(false);

    clVibratoLFO.setMinAmount(-1.0f);
    clVibratoLFO.setMaxAmount(1.0f);
    clVibratoLFO.setEnabled(false);
}

void RuneWerkRechteckVoice::setSampleRate(float fs)
{
    fSmpFreq = fs;

    clRuneWerkRechteck.setSampleRate(fSmpFreq);
    clEnv.setSampleRate(fSmpFreq);
    clEnvFilter.setSampleRate(fSmpFreq);
    clPWMLFO.init(fSmpFreq);
    clTremoloLFO.init(fSmpFreq);
    clVibratoLFO.init(fSmpFreq);
}

void RuneWerkRechteckVoice::getOutputSample(float* outL, float* outR)
{
    float l, r;

    clRuneWerkRechteck.getOutputSample(&l, &r);

    if(clPWMLFO.getEnabled() && clRuneWerkRechteck.getMode() == RWRO_MODE_PULSE)
        clRuneWerkRechteck.setPulseWidth(clPWMLFO.getValue());

    if(clTremoloLFO.getEnabled())
    {
        l *= clTremoloLFO.getValue();
        r *= clTremoloLFO.getValue();
    }

    if(clVibratoLFO.getEnabled())
        clRuneWerkRechteck.setMasterFineTune(clVibratoLFO.getValue());
    else
        clRuneWerkRechteck.setMasterFineTune(0.0f);

    clEnvFilter.process(&l, &r);

    (*outL) = l * clEnv.getSample();
    (*outR) = r * clEnv.getSample();
}
