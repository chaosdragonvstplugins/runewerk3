/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "tempoinput.h"

/***
C++ FILE:
Tempo Input
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

TempoInput::TempoInput()
{
}

TempoInput::~TempoInput()
{
}

void TempoInput::init()
{
    iTx100 = 1;
    iTx10 = 2;
    iTx1 = 0;
}

void TempoInput::setTempox100(int v){iTx100 = v % 10;}
void TempoInput::setTempox10(int v){iTx10 = v % 10;}
void TempoInput::setTempox1(int v){iTx1 = v % 10;}

int TempoInput::getTempox100(){return iTx100;}
int TempoInput::getTempox10(){return iTx10;}
int TempoInput::getTempox1(){return iTx1;}

int TempoInput::getValue(){return iTx100 * 100 + iTx10 * 10 + iTx1;}
