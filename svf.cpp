/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "svf.h"

/***
C++ FILE:
State Variable Filter
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

float StateVariableFilter::calc_cutoff(float f)
{
    float t = f * fInvSampleRate * TWO_PI;
    float q = t * t;
    float u = t;
    float S = u;

    u = u * q * 0.16666666f;
    S -= u;

    u = u * q * 0.05f;
    S += u;

    return S;
}

StateVariableFilter::StateVariableFilter()
{
}

StateVariableFilter::~StateVariableFilter()
{
}

void StateVariableFilter::initFilter(float fs)
{
	fSampleRate = fs;
	fInvSampleRate = 1.0f / fSampleRate;
	eMode = SVF_LP;
	fCalculatedCutoff = 1000.0f;
	fQ = 1.0f;
    fZDelay[0] = 0;
	fZDelay[1] = 0;
}

void StateVariableFilter::setSampleRate(float fs)
{
	fSampleRate = fs;
}

void StateVariableFilter::setMode(int m)
{
    eMode = m;
}

void StateVariableFilter::setCutoff(float fc)
{
	if(fc < 10.0f)
		fCutoff = 10.0f;
	else if(fc > fSampleRate * 0.1f)
		fCutoff = fSampleRate * 0.1f;
	else
		fCutoff = fc;

	fCalculatedCutoff = calc_cutoff(fCutoff);
}

void StateVariableFilter::setQ(float fq)
{
	fQ = fq;
}

int StateVariableFilter::getMode()
{
    return eMode;
}

float StateVariableFilter::getCutoff()
{
	return fCutoff;
}

float StateVariableFilter::getQ()
{
	return fQ;
}

float StateVariableFilter::process(float x)
{
	fInputSample = x;

    fLowPassIO = fZDelay[1] + fZDelay[0] * fCalculatedCutoff;
	fHighPassIO = fInputSample - fLowPassIO - fZDelay[0] * fQ;
	fBandPassIO = fHighPassIO * fCalculatedCutoff + fZDelay[0];
	fBandStopIO = fLowPassIO + fHighPassIO;
	fZDelay[0] = fBandPassIO;
	fZDelay[1] = fLowPassIO;

	switch(eMode)
	{
		case SVF_LP:
			fOutputSample = fLowPassIO;
			break;

		case SVF_HP:
			fOutputSample = fHighPassIO;
			break;

		case SVF_BP:
			fOutputSample = fBandPassIO;
			break;

		case SVF_BS:
			fOutputSample = fBandStopIO;
			break;
	}

	return fOutputSample;
}
