/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_ADSR_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_ADSR_HFILE

/***
HEADER FILE:
Amplitude Envelope
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

enum
{
    RWCENV_STAGE_ATTACK = 0,
    RWCENV_STAGE_DECAY,
    RWCENV_STAGE_SUSTAIN,
    RWCENV_STAGE_RELEASE,
    RWCENV_STAGE_FINISHED,
};

class ADSREnvelope
{
	private:
		float fAttack, fDecay, fSustain, fRelease;
		float fInvAttack, fInvDecay, fInvRelease;
		float fTime;
		float fSample, fSampleZ;
		float fSampleRate, fInvSampleRate;
		int eStage;
		bool isSustainLoopOn;

	public:
		ADSREnvelope();

		void Reset();

		void setSampleRate(float);

		void setAttack(float);
		void setDecay(float);
		void setSustain(float);
		void setRelease(float);
		void setStage(int);
		void setSustainLoopEnabled(bool);
		void setFinished();
		void setReleaseStage();

		float getAttack();
		float getDecay();
		float getSustain();
		float getRelease();
		int getStage();
		bool getFinished();

		float getSample();
};

#endif
