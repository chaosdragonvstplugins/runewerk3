/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_FMDRAGON_RUNEWERK_BPMSYNC_HFILE
#define LUIGI_ELETTRICO_FMDRAGON_RUNEWERK_BPMSYNC_HFILE

/***
HEADER FILE:
BPM Synchronizer
for FMDragon and RuneWerk synthesizer plugins
(c) Luigi Elettrico
***/

#include "mathfunc.h"

class BPMSync
{
    private:
        int iBPM, iNumerator, iDenominator;
        float fRealRate;

    public:
        BPMSync();
        ~BPMSync();

        void init();

        void setBPM(int);
        void setBeatSyncNumerator(int);
        void setBeatSyncDenominator(int);

        int getBPM();
        int getBeatSyncNumerator();
        int getBeatSyncDenominator();

        float getRealRate();
};

#endif
