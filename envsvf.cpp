/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "envsvf.h"

/***
C++ FILE:
State Variable Filter with Amplitude Envelope
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

EnvSVF::EnvSVF()
{
}

EnvSVF::~EnvSVF()
{
}

void EnvSVF::init(float fs)
{
    clSVF.initFilter(fs);
    clEnv.setSampleRate(fs);
    fCutoff = clSVF.getCutoff();
    fReso = clSVF.getQ();
}

void EnvSVF::setSampleRate(float fs)
{
    clSVF.setSampleRate(fs);
    clEnv.setSampleRate(fs);
}

void EnvSVF::Reset(){clEnv.Reset();}
void EnvSVF::setAttack(float v){clEnv.setAttack(v);}
void EnvSVF::setDecay(float v){clEnv.setDecay(v);}
void EnvSVF::setSustain(float v){clEnv.setSustain(v);}
void EnvSVF::setRelease(float v){clEnv.setRelease(v);}
void EnvSVF::setSustainLoopEnabled(bool e){clEnv.setSustainLoopEnabled(e);}
void EnvSVF::setReleaseStage(){clEnv.setReleaseStage();}
void EnvSVF::setFinished(){clEnv.setFinished();}

void EnvSVF::setMode(int m)
{
    clSVF.setMode(m);
    clSVF.setCutoff(fCutoff);
    clSVF.setQ(fReso);
}

void EnvSVF::setCutoff(float v)
{
    clSVF.setCutoff(v);
    fCutoff = clSVF.getCutoff();
}

void EnvSVF::setReso(float v)
{
    clSVF.setQ(v);
    fReso = clSVF.getQ();
}

float EnvSVF::getAttack(){return clEnv.getAttack();}
float EnvSVF::getDecay(){return clEnv.getDecay();}
float EnvSVF::getSustain(){return clEnv.getSustain();}
float EnvSVF::getRelease(){return clEnv.getRelease();}
bool EnvSVF::getFinished(){return clEnv.getFinished();}

int EnvSVF::getMode(){return clSVF.getMode();}
float EnvSVF::getCutoff(){return fCutoff;}
float EnvSVF::getReso(){return fReso;}

void EnvSVF::process(float* inL, float* inR)
{
    clSVF.setCutoff(fCutoff * clEnv.getSample());

    *inL = clSVF.process(*inL);
    *inR = clSVF.process(*inR);
}
