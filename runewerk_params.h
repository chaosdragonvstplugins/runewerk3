/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_RECHTECK_PARAMS_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_RECHTECK_PARAMS_HFILE

/***
HEADER FILE:
Configurable Parameters
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

enum eRuneWerkRechteckVSTParameters
{
    PARAM_NOISE,
    PARAM_PWIDTH,
    PARAM_PWMENA,
    PARAM_PWMMIN,
    PARAM_PWMMAX,
    PARAM_PWMRTE,
    PARAM_PWMTRG,
    PARAM_FINE,
    PARAM_ATK,
    PARAM_DCY,
    PARAM_STN,
    PARAM_RLS,
    PARAM_GAIN,
    PARAM_FILTER,
    PARAM_CUTOFF,
    PARAM_RESO,
    PARAM_FLTATK,
    PARAM_FLTDCY,
    PARAM_FLTSTN,
    PARAM_FLTRLS,
    PARAM_TRMENA,
    PARAM_TRMAMT,
    PARAM_TRMRTE,
    PARAM_BPM100,
    PARAM_BPM010,
    PARAM_BPM001,
    PARAM_VIBENA,
    PARAM_VIBAMT,
    PARAM_VIBRTE,

    NUMPARAMS,
};

enum eRuneWerkRechteckVSTPrograms
{
    PROG_NONE,
    NUMPROGRAMS,
};

extern const char* a_strRuneWerkRechteckVSTParameterNames[NUMPARAMS];
extern const char* a_strRuneWerkRechteckVSTParameterLabels[NUMPARAMS];

#endif
