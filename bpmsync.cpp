/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "bpmsync.h"

/***
C++ FILE:
BPM Synchronizer
for RuneWerk synthesizer plugins
(c) Luigi Elettrico
***/

BPMSync::BPMSync()
{
}

BPMSync::~BPMSync()
{
}

void BPMSync::init(){iNumerator = 1; iDenominator = 1;}

void BPMSync::setBPM(int v){iBPM = v;}
void BPMSync::setBeatSyncNumerator(int v){iNumerator = (v < 1 ? 1 : v);}
void BPMSync::setBeatSyncDenominator(int v){iDenominator = (v < 1 ? 1 : v);}

int BPMSync::getBPM(){return iBPM;}
int BPMSync::getBeatSyncNumerator(){return iNumerator;}
int BPMSync::getBeatSyncDenominator(){return iDenominator;}

float BPMSync::getRealRate()
{
    fRealRate = ((float)iDenominator * (float)iBPM * INV_60) / (float)iNumerator;
    return fRealRate;
}
