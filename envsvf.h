/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_ENVSVF_CLASS_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_ENVSVF_CLASS_HFILE

/***
HEADER FILE:
Signal Envelope for State Variable Filter
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

#include "svf.h"
#include "adsr.h"

class EnvSVF
{
    private:
        StateVariableFilter clSVF;

        ADSREnvelope clEnv;

        float fCutoff, fReso;
        float fSampleRate;

    public:
        EnvSVF();
        ~EnvSVF();

        void init(float fs);
        void setSampleRate(float fs);

        void Reset();
        void setAttack(float v);
        void setDecay(float v);
        void setSustain(float v);
        void setRelease(float v);
        void setSustainLoopEnabled(bool);
        void setReleaseStage();
        void setFinished();

        void setMode(int m);
        void setCutoff(float v);
        void setReso(float v);

        float getAttack();
        float getDecay();
        float getSustain();
        float getRelease();
        bool getFinished();

        int getMode();
        float getCutoff();
        float getReso();

        void process(float* inL, float* inR);

};

#endif
