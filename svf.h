/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_SVF_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_SVF_HFILE

/***
HEADER FILE:
State Variable Filter
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

#include "mathfunc.h"

enum SVFMODE
{
    SVF_LP,
    SVF_HP,
	SVF_BP,
	SVF_BS,
};

class StateVariableFilter
{
    private:
        MathFunc clMathFunc;
		int eMode;
		float fSampleRate, fInvSampleRate;
		float fCutoff, fCalculatedCutoff, fQ;
		float fInputSample, fOutputSample;
		float fLowPassIO, fHighPassIO, fBandPassIO, fBandStopIO;
		float fZDelay[2];

		float calc_cutoff(float f);

    public:
        StateVariableFilter();
        ~StateVariableFilter();

        void    initFilter(float fs);
		void	setSampleRate(float fs);
		void    setMode(int m);
		void	setCutoff(float fc);
		void	setQ(float fq);

		int     getMode();
		float	getCutoff();
		float	getQ();

        float   process(float x);
};

#endif
