/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_RECHTECK_MAIN_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_RECHTECK_MAIN_HFILE

/***
HEADER FILE:
Main Program
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

/// utilities
#include <cstdio>

/// runewerk synth voice
#include "voice.h"

/// vst sdk
#include "audioeffectx.h"

/// parameters
#include "runewerk_params.h"

/// midi note to frequency conversion
#include "midi2f.h"

/// tempo input, because timeinfo fucks up
#include "tempoinput.h"

/// version
#define RUNEWERK_VERSION 0x0103

/// unique id
#define RUNEWERK_UID 0x31335752

#define RWE_MAXVOICES 32
#define RWE_INVMAXVOICES (1.0f / (float)RWE_MAXVOICES)
#define RWE_NOTE_NOTFOUND -1

/// RuneWerkRechteckVST class
class RuneWerkRechteckVST: public AudioEffectX
{
    private:
        float fValueTab[NUMPARAMS];

        RuneWerkRechteckVoice clVoice[RWE_MAXVOICES];

        MathFunc clMathFunc;

        TempoInput clTempo;

        bool    isPWMTriggered;

        void    Init();

        void    ParamsToNormal();
        void    NormalToParams();

        void    noteOn(VstInt32 note, VstInt32 velocity, int v);
        void    noteOff(int v);

        int     FindNoteOnVoiceID(int note);
        int     FindFirstNoteOffVoiceID();
        void    AllNoteOff();

        void    updateTempo();
    public:
        RuneWerkRechteckVST(audioMasterCallback audioMaster);
        ~RuneWerkRechteckVST();

        /// Process
        virtual void        processReplacing(float **Ins, float **Outs, VstInt32 SmpFrames);
        virtual VstInt32    processEvents(VstEvents* E);

        /// Get
        virtual float       getParameter(VstInt32 index);
        virtual void        getParameterDisplay(VstInt32 index, char *text);
        virtual void        getParameterLabel(VstInt32 index, char *text);
        virtual void        getParameterName(VstInt32 index, char *text);
        virtual bool        getOutputProperties(VstInt32 index, VstPinProperties* properties);
        virtual bool        getEffectName(char* name);
        virtual bool        getVendorString(char* text);
        virtual bool        getProductString(char* text);
        virtual VstInt32    getVendorVersion();

        /// Set
        virtual void        setSampleRate(float sampleRate);
        virtual void        setBlockSize(VstInt32 blockSize);
        virtual void        setParameter(VstInt32 index, float value);
};

#endif
