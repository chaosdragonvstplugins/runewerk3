/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_RECHTECK_SYNTH_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_RECHTECK_SYNTH_HFILE

/***
HEADER FILE:
Rectangle Wave Synthesizer
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

#include "runewerk_osc.h"

class RuneWerkRechteckSynth
{
    private:
        float fGain;
        float fVel;

        RuneWerkRechteckOsc clRuneWerkRechteckOsc;

    public:
        RuneWerkRechteckSynth();
        ~RuneWerkRechteckSynth();

        void Init(float fs);

        void setSampleRate(float fs);

        void setMode(int m);
        void setPulseWidth(float v);
        void setFreq(float v);
        void setFineTune(float v);
        void setMasterFineTune(float v);
        void setGain(float v);
        void setVel(int v);

        float getSampleRate();

        int getMode();
        float getPulseWidth();
        float getFreq();
        float getFineTune();
        float getMasterFineTune();
        float getGain();

        void getOutputSample(float *outL, float *outR);
};

#endif
