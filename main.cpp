/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "main.h"

/***
C++ FILE:
Main Program
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

const char* a_strRuneWerkRechteckVSTParameterNames[NUMPARAMS] = {
    "Mode",
    "Pulse width",
    "PWM",
    "PWM min. value",
    "PWM max. value",
    "PWM rate",
    "PWM retrigger",
    "Fine tune",
    "Attack",
    "Decay",
    "Sustain",
    "Release",
    "Gain",
    "Filter",
    "Filter cutoff",
    "Filter reso",
    "Filter attack",
    "Filter decay",
    "Filter sustain",
    "Filter release",
    "Tremolo enable",
    "Tremolo amount",
    "Tremolo rate",
    "Tempo n x 100",
    "Tempo n x 10",
    "Tempo n x 1",
    "Vibrato enable",
    "Vibrato amount",
    "Vibrato rate",
};

const char* a_strRuneWerkRechteckVSTParameterLabels[NUMPARAMS] = {
    "",
    "%",
    "",
    "%",
    "%",
    "x Beat",
    "",
    "ht",
    "s",
    "s",
    "",
    "s",
    "dB",
    "",
    "Hz",
    "",
    "s",
    "s",
    "",
    "s",
    "",
    "",
    "x Beat",
    "BPM",
    "BPM",
    "BPM",
    "",
    "",
    "x Beat",
};

const char* a_strFilterMode[] = {
    "LP",
    "HP",
    "BP",
    "BS",
};

RuneWerkRechteckVST::RuneWerkRechteckVST(audioMasterCallback audioMaster) : AudioEffectX(audioMaster, NUMPROGRAMS, NUMPARAMS)
{
    isSynth(true);
    setNumInputs(0);
    setNumOutputs(2);
    canProcessReplacing(true);
    setUniqueID(RUNEWERK_UID);

    Init();
}

RuneWerkRechteckVST::~RuneWerkRechteckVST()
{
}

AudioEffect *createEffectInstance(audioMasterCallback audioMaster)
{
    return new RuneWerkRechteckVST(audioMaster);
}

void RuneWerkRechteckVST::processReplacing(float **Ins, float **Outs, VstInt32 SmpFrames)
{
    float* yL = Outs[0];
	float* yR = Outs[1];
	float a, b;
	float l, r;

    while(--SmpFrames >= 0)
    {
        l = 0;
        r = 0;

        for(int v = 0; v < RWE_MAXVOICES; v++)
        {
            if(!clVoice[v].clEnv.getFinished())
            {
                clVoice[v].getOutputSample(&a, &b);
                l += a;
                r += b;
            }
        }

        *(yL++) = l;
        *(yR++) = r;
    }
}

int RuneWerkRechteckVST::FindNoteOnVoiceID(int note)
{
    for(int v = 0; v < RWE_MAXVOICES; v++)
        if(clVoice[v].nNote == note && clVoice[v].isNoteOn && !clVoice[v].clEnv.getFinished())
            return v;

    return RWE_NOTE_NOTFOUND;
}

int RuneWerkRechteckVST::FindFirstNoteOffVoiceID()
{
    for(int v = 0; v < RWE_MAXVOICES; v++)
        if(!clVoice[v].isNoteOn && clVoice[v].clEnv.getFinished())
            return v;

    return RWE_NOTE_NOTFOUND;
}

void RuneWerkRechteckVST::AllNoteOff()
{
    for(int v = 0; v < RWE_MAXVOICES; ++v)
        noteOff(v);
}

VstInt32 RuneWerkRechteckVST::processEvents(VstEvents* E)
{
    VstInt32 note;
    VstInt32 velocity;
    int v;

    for(VstInt32 i = 0; i < E->numEvents; ++i)
    {
        if((E->events[i])->type != kVstMidiType)
            continue;

        VstMidiEvent* event = (VstMidiEvent*)E->events[i];
        char* midiData = event->midiData;

        VstInt32 status = midiData[0] & 0xf0;

        if(status == 0x80)
        {
            note = midiData[1] & 0x7f;
            velocity = 0;

            if((v = FindNoteOnVoiceID(note)) != RWE_NOTE_NOTFOUND)
                noteOff(v);
        }
        else if(status == 0x90)
        {
            note = midiData[1] & 0x7f;
            velocity = midiData[2] & 0x7f;

            if((v = FindFirstNoteOffVoiceID()) != RWE_NOTE_NOTFOUND)
                noteOn(note, velocity, v);
        }
        else if(status == 0xb0)
        {
            if(midiData[1] == 0x7e || midiData[1] == 0x7b)
            {
                AllNoteOff();
            }
        }
        else
            break;

        event++;
    }

	return 1;
}

float RuneWerkRechteckVST::getParameter(VstInt32 index)
{
    return fValueTab[index];
}

void RuneWerkRechteckVST::getParameterDisplay(VstInt32 index, char *text)
{
    char strDisp[16];
    memset(strDisp, 0, 16);

    switch(index)
    {
        case PARAM_NOISE: sprintf(strDisp, "%s", clVoice[0].clRuneWerkRechteck.getMode() == RWRO_MODE_PULSE ? "Pulse" : "Noise"); break;
        case PARAM_PWIDTH: sprintf(strDisp, "%3.0d", (int)(clVoice[0].clRuneWerkRechteck.getPulseWidth() * 100.0f)); break;
        case PARAM_PWMENA: sprintf(strDisp, "%s", clVoice[0].clPWMLFO.getEnabled() ? "On" : "Off"); break;
        case PARAM_PWMMIN: sprintf(strDisp, "%3.0d", (int)(clVoice[0].clPWMLFO.getMinAmount() * 100.0f)); break;
        case PARAM_PWMMAX: sprintf(strDisp, "%3.0d", (int)(clVoice[0].clPWMLFO.getMaxAmount() * 100.0f)); break;
        case PARAM_PWMRTE: sprintf(strDisp, "%d/%d", clVoice[0].clPWMLFO.getRateNumerator(), clVoice[0].clPWMLFO.getRateDenominator()); break;
        case PARAM_PWMTRG: sprintf(strDisp, "%s", isPWMTriggered ? "On" : "Off"); break;
        case PARAM_FINE: sprintf(strDisp, "%.2f", clVoice[0].clRuneWerkRechteck.getFineTune()); break;
        case PARAM_ATK: sprintf(strDisp, "%.2f", clVoice[0].clEnv.getAttack()); break;
        case PARAM_DCY: sprintf(strDisp, "%.2f", clVoice[0].clEnv.getDecay()); break;
        case PARAM_STN: sprintf(strDisp, "%.2f", clVoice[0].clEnv.getSustain()); break;
        case PARAM_RLS: sprintf(strDisp, "%.2f", clVoice[0].clEnv.getRelease()); break;
        case PARAM_GAIN: sprintf(strDisp, "%.2f", clMathFunc.V2dB(clVoice[0].clRuneWerkRechteck.getGain())); break;
        case PARAM_FILTER: sprintf(strDisp, "%s", a_strFilterMode[clVoice[0].clEnvFilter.getMode()]); break;
        case PARAM_CUTOFF: sprintf(strDisp, "%.2f", clVoice[0].clEnvFilter.getCutoff()); break;
        case PARAM_RESO: sprintf(strDisp, "%.2f", clVoice[0].clEnvFilter.getReso()); break;
        case PARAM_FLTATK: sprintf(strDisp, "%.2f", clVoice[0].clEnvFilter.getAttack()); break;
        case PARAM_FLTDCY: sprintf(strDisp, "%.2f", clVoice[0].clEnvFilter.getDecay()); break;
        case PARAM_FLTSTN: sprintf(strDisp, "%.2f", clVoice[0].clEnvFilter.getSustain()); break;
        case PARAM_FLTRLS: sprintf(strDisp, "%.2f", clVoice[0].clEnvFilter.getRelease()); break;
        case PARAM_TRMENA: sprintf(strDisp, "%s", clVoice[0].clTremoloLFO.getEnabled() ? "On" : "Off"); break;
        case PARAM_TRMAMT: sprintf(strDisp, "%.2f", 1.0f - clVoice[0].clTremoloLFO.getMinAmount()); break;
        case PARAM_TRMRTE: sprintf(strDisp, "%d/%d", clVoice[0].clTremoloLFO.getRateNumerator(), clVoice[0].clTremoloLFO.getRateDenominator()); break;
        case PARAM_BPM100:
        case PARAM_BPM010:
        case PARAM_BPM001: sprintf(strDisp, "%d", clTempo.getValue()); break;
        case PARAM_VIBENA: sprintf(strDisp, "%s", clVoice[0].clVibratoLFO.getEnabled() ? "On" : "Off"); break;
        case PARAM_VIBAMT: sprintf(strDisp, "%.2f", clVoice[0].clVibratoLFO.getMaxAmount()); break;
        case PARAM_VIBRTE: sprintf(strDisp, "%d/%d", clVoice[0].clVibratoLFO.getRateNumerator(), clVoice[0].clVibratoLFO.getRateDenominator()); break;
        default: strcpy(text, ""); break;
    }

    strcpy(text, (char*)strDisp);
}

void RuneWerkRechteckVST::getParameterLabel(VstInt32 index, char *text)
{
    if(index >= 0 && index < NUMPARAMS)
        strcpy(text, a_strRuneWerkRechteckVSTParameterLabels[index]);
    else
        strcpy(text, "");
}

void RuneWerkRechteckVST::getParameterName(VstInt32 index, char *text)
{
    if(index >= 0 && index < NUMPARAMS)
        strcpy(text, a_strRuneWerkRechteckVSTParameterNames[index]);
    else
        strcpy(text, "");
}

bool RuneWerkRechteckVST::getOutputProperties(VstInt32 index, VstPinProperties* properties)
{
    if(index < 2)
    {
        if(index)
            sprintf(properties->label, "RuneWerk 3.1 VST R");
        else
            sprintf(properties->label, "RuneWerk 3.1 VST L");

        properties->flags = kVstPinIsActive;

        if(index < 2)
            properties->flags |= kVstPinIsStereo;

        return true;
    }
    return false;
}

bool RuneWerkRechteckVST::getEffectName(char* name)
{
    strcpy(name, "RuneWerk 3.1 VST");
    return true;
}

bool RuneWerkRechteckVST::getVendorString(char* text)
{
    strcpy(text, "Luigi Elettrico");
    return true;
}

bool RuneWerkRechteckVST::getProductString(char* text)
{
    strcpy(text, "RuneWerk 3.1 VST");
    return true;
}

VstInt32 RuneWerkRechteckVST::getVendorVersion()
{
    return RUNEWERK_VERSION;
}

void RuneWerkRechteckVST::setSampleRate(float sampleRate)
{
    AudioEffectX::setSampleRate(sampleRate);

    for(int v = 0; v < RWE_MAXVOICES; v++)
    {
        clVoice[v].setSampleRate(sampleRate);
    }
}

void RuneWerkRechteckVST::setBlockSize(VstInt32 blockSize)
{
    AudioEffectX::setBlockSize(blockSize);
}

void RuneWerkRechteckVST::setParameter(VstInt32 index, float value)
{
    fValueTab[index] = value;

    NormalToParams();

    if(index == PARAM_BPM100 || index == PARAM_BPM010 || index == PARAM_BPM001)
        updateTempo();
}

void RuneWerkRechteckVST::updateTempo()
{
    for(int v = 0; v < RWE_MAXVOICES; v++)
    {
        clVoice[v].clTremoloLFO.setBPM(clTempo.getValue());
        clVoice[v].clVibratoLFO.setBPM(clTempo.getValue());
        clVoice[v].clPWMLFO.setBPM(clTempo.getValue());
    }
}

void RuneWerkRechteckVST::Init()
{
    for(int v = 0; v < RWE_MAXVOICES; v++)
        clVoice[v].init(getSampleRate());

    isPWMTriggered = false;

    clTempo.init();
    updateTempo();

    ParamsToNormal();
}

void RuneWerkRechteckVST::noteOn(VstInt32 note, VstInt32 velocity, int v)
{
    if(isPWMTriggered && clVoice[v].clPWMLFO.getEnabled())
        clVoice[v].clPWMLFO.resetPhase();

    clVoice[v].isNoteOn = true;
    clVoice[v].nNote = note;
    clVoice[v].nVel = velocity;

    clVoice[v].clRuneWerkRechteck.setFreq(midi2freq(note));
    clVoice[v].clRuneWerkRechteck.setVel(velocity);

    clVoice[v].clEnv.Reset();
    clVoice[v].clEnvFilter.Reset();

    clVoice[v].clEnv.setSustainLoopEnabled(true);
    clVoice[v].clEnvFilter.setSustainLoopEnabled(true);

    clVoice[v].clTremoloLFO.resetPhase();
    clVoice[v].clVibratoLFO.resetPhase();
}

void RuneWerkRechteckVST::noteOff(int v)
{
	clVoice[v].isNoteOn = false;
	clVoice[v].clEnv.setReleaseStage();
	clVoice[v].clEnv.setSustainLoopEnabled(false);
	clVoice[v].clEnvFilter.setReleaseStage();
	clVoice[v].clEnvFilter.setSustainLoopEnabled(false);

}

void RuneWerkRechteckVST::ParamsToNormal()
{
    fValueTab[PARAM_NOISE] = (float)clVoice[0].clRuneWerkRechteck.getMode();
    fValueTab[PARAM_PWIDTH] = clVoice[0].clRuneWerkRechteck.getPulseWidth();
    fValueTab[PARAM_PWMENA] = (float)clVoice[0].clPWMLFO.getEnabled();
    fValueTab[PARAM_PWMMIN] = clVoice[0].clPWMLFO.getMinAmount();
    fValueTab[PARAM_PWMMAX] = clVoice[0].clPWMLFO.getMaxAmount();
    fValueTab[PARAM_PWMRTE] = (float)clVoice[0].clPWMLFO.getSyncRateE() / (float)(LFOR_NUMRATES - 1);
    fValueTab[PARAM_FINE] = (clVoice[0].clRuneWerkRechteck.getFineTune() + 1.0f) * 0.5f;

    fValueTab[PARAM_ATK] = (clVoice[0].clEnv.getAttack() - 0.001f) * 0.1f;
    fValueTab[PARAM_DCY] = (clVoice[0].clEnv.getDecay() - 0.001f) * 0.5f;
    fValueTab[PARAM_STN] = clVoice[0].clEnv.getSustain();
    fValueTab[PARAM_RLS] = (clVoice[0].clEnv.getRelease() - 0.001f) * 0.1f;

    fValueTab[PARAM_GAIN] = clVoice[0].clRuneWerkRechteck.getGain() * 0.5f;

    fValueTab[PARAM_FILTER] = (float)(clVoice[0].clEnvFilter.getMode()) * INV_3;
    fValueTab[PARAM_CUTOFF] = clMathFunc.Log10Scale(clVoice[0].clEnvFilter.getCutoff(), 3.0f, getSampleRate() * 0.5f);
    fValueTab[PARAM_RESO] = clVoice[0].clEnvFilter.getReso() * 0.5f;

    fValueTab[PARAM_FLTATK] = (clVoice[0].clEnvFilter.getAttack() - 0.001f) * 0.1f;
    fValueTab[PARAM_FLTDCY] = (clVoice[0].clEnvFilter.getDecay() - 0.001f) * 0.5f;
    fValueTab[PARAM_FLTSTN] = clVoice[0].clEnvFilter.getSustain();
    fValueTab[PARAM_FLTRLS] = (clVoice[0].clEnvFilter.getRelease() - 0.001f) * 0.1f;

    fValueTab[PARAM_TRMENA] = (float)clVoice[0].clTremoloLFO.getEnabled();
    fValueTab[PARAM_TRMAMT] = 1.0f - clVoice[0].clTremoloLFO.getMinAmount();
    fValueTab[PARAM_TRMRTE] = (float)clVoice[0].clTremoloLFO.getSyncRateE() / (float)(LFOR_NUMRATES - 1);

    fValueTab[PARAM_BPM100] = ((float)clTempo.getTempox100()) * INV_9;
    fValueTab[PARAM_BPM010] = ((float)clTempo.getTempox10()) * INV_9;
    fValueTab[PARAM_BPM001] = ((float)clTempo.getTempox1()) * INV_9;

    fValueTab[PARAM_VIBENA] = (float)clVoice[0].clVibratoLFO.getEnabled();
    fValueTab[PARAM_VIBAMT] = clVoice[0].clVibratoLFO.getMaxAmount();
    fValueTab[PARAM_VIBRTE] = (float)clVoice[0].clVibratoLFO.getSyncRateE() / (float)(LFOR_NUMRATES - 1);

    fValueTab[PARAM_PWMTRG] = (float)isPWMTriggered;
}

void RuneWerkRechteckVST::NormalToParams()
{
    for(int v = 0; v < RWE_MAXVOICES; v++)
    {
        clVoice[v].clRuneWerkRechteck.setMode((int)fValueTab[PARAM_NOISE]);
        clVoice[v].clRuneWerkRechteck.setPulseWidth(fValueTab[PARAM_PWIDTH]);
        clVoice[v].clPWMLFO.setEnabled((int)fValueTab[PARAM_PWMENA]);
        clVoice[v].clPWMLFO.setMinAmount(fValueTab[PARAM_PWMMIN]);
        clVoice[v].clPWMLFO.setMaxAmount(fValueTab[PARAM_PWMMAX]);
        clVoice[v].clPWMLFO.setSyncRateE((int)(fValueTab[PARAM_PWMRTE] * (float)(LFOR_NUMRATES - 1)));
        clVoice[v].clRuneWerkRechteck.setFineTune(fValueTab[PARAM_FINE] * 2.0f - 1.0f);

        clVoice[v].clEnv.setAttack(fValueTab[PARAM_ATK] * 10 + 0.001f);
        clVoice[v].clEnv.setDecay(fValueTab[PARAM_DCY] * 2 + 0.001f);
        clVoice[v].clEnv.setSustain(fValueTab[PARAM_STN]);
        clVoice[v].clEnv.setRelease(fValueTab[PARAM_RLS] * 10 + 0.001f);
        clVoice[v].clRuneWerkRechteck.setGain(fValueTab[PARAM_GAIN] * 2);

        clVoice[v].clEnvFilter.setMode((int)(fValueTab[PARAM_FILTER] * 3.0f));
        clVoice[v].clEnvFilter.setCutoff(clMathFunc.Exp10Scale(fValueTab[PARAM_CUTOFF], 3.0f, getSampleRate() * 0.5f));
        clVoice[v].clEnvFilter.setReso(fValueTab[PARAM_RESO] * 2.0f);

        clVoice[v].clEnvFilter.setAttack(fValueTab[PARAM_FLTATK] * 10 + 0.001f);
        clVoice[v].clEnvFilter.setDecay(fValueTab[PARAM_FLTDCY] * 2 + 0.001f);
        clVoice[v].clEnvFilter.setSustain(fValueTab[PARAM_FLTSTN]);
        clVoice[v].clEnvFilter.setRelease(fValueTab[PARAM_FLTRLS] * 10 + 0.001f);

        clVoice[v].clTremoloLFO.setEnabled((int)fValueTab[PARAM_TRMENA]);
        clVoice[v].clTremoloLFO.setMinAmount(1.0f - fValueTab[PARAM_TRMAMT]);
        clVoice[v].clTremoloLFO.setSyncRateE((int)(fValueTab[PARAM_TRMRTE] * (float)(LFOR_NUMRATES - 1)));

        clVoice[v].clVibratoLFO.setEnabled((int)fValueTab[PARAM_VIBENA]);
        clVoice[v].clVibratoLFO.setMaxAmount(fValueTab[PARAM_VIBAMT]); clVoice[v].clVibratoLFO.setMinAmount(-fValueTab[PARAM_VIBAMT]);
        clVoice[v].clVibratoLFO.setSyncRateE((int)(fValueTab[PARAM_VIBRTE] * (float)(LFOR_NUMRATES - 1)));
    }

    isPWMTriggered = (bool)fValueTab[PARAM_PWMTRG];

    clTempo.setTempox100((int)(fValueTab[PARAM_BPM100] * 9.0f));
    clTempo.setTempox10((int)(fValueTab[PARAM_BPM010] * 9.0f));
    clTempo.setTempox1((int)(fValueTab[PARAM_BPM001] * 9.0f));
}
