/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#ifndef LUIGI_ELETTRICO_RUNEWERK_RECHTECK_VOICE_HFILE
#define LUIGI_ELETTRICO_RUNEWERK_RECHTECK_VOICE_HFILE

/***
HEADER FILE:
Rectangle Synthesizer Voice
for RuneWerk synthesizer plugin
(c) Luigi Elettrico
***/

#include "audioeffectx.h"
#include "runewerk_synth.h"
#include "adsr.h"
#include "envsvf.h"
#include "lfo.h"

class RuneWerkRechteckVoice
{
    private:
        float fSmpFreq;

    public:
        RuneWerkRechteckSynth clRuneWerkRechteck;
        ADSREnvelope clEnv;
        EnvSVF clEnvFilter;
        LowFreqOsc clPWMLFO;
        LowFreqOsc clTremoloLFO;
        LowFreqOsc clVibratoLFO;

        VstInt32 nNote;
        VstInt32 nVel;
        bool isNoteOn;

        void init(float);
        void setSampleRate(float);

        void getOutputSample(float*, float*);
};

#endif
