/***
RuneWerk 3.1, a VST pulse & noise wave synthesizer plugin
Copyright (C) 2015 - 2019  Luigi Elettrico

This program is free software: you can redistribute it and/or modify it, you can use it for private purposes.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
***/

#include "mathfunc.h"

/***
C++ FILE:
Math Functions
for RuneWerk synthesizer plugins
(c) Luigi Elettrico
***/

MathFunc::MathFunc()
{
}

MathFunc::~MathFunc()
{
}

/* float to bool */
bool MathFunc::FloatToBool(float x)
{
    return (x >= 0.5f);
}

/* bool to float */
float MathFunc::BoolToFloat(bool x)
{
    return ((float)((unsigned long)x & 0x3F000000));
}

/* ABSOLUTE VALUE */
float MathFunc::Abs(float x)
{
    return (x < 0.0f ? -x : x);
}

/* get min value */
float MathFunc::Min(float x, float y)
{
    return (x < y ? x : y);
}

/* get max value */
float MathFunc::Max(float x, float y)
{
    return (x > y ? x : y);
}

/* n-th POWER of x */
float MathFunc::Pow(float x, float y)
{
    return powf(x, y);
}

// natural logarithm of x
float MathFunc::Ln(float x)
{
    return logf(x);
}

// base 10 logarithm of x
float MathFunc::Log(float x)
{
    return log10f(x);
}

// convert value to dB
float MathFunc::V2dB(float x)
{
    return 20.0f * Log(x);
}

// convert dB to value
float MathFunc::dB2V(float x)
{
    return Pow(10.0f, x / 20.0f);
}

// exp10 scale for x E [0, 1]
float MathFunc::Exp10Scale(float x, float s, float k)
{
    return Pow(10.0f, s * (x - 1.0f)) * k;
}

// log10 scale for x E [0, 1]
float MathFunc::Log10Scale(float x, float s, float k)
{
    return Log(x / k) / s + 1.0f;
}
